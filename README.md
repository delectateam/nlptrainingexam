# CONTENTS
-----------

* resources/reviews.json ==> restaurant user reviews bodies and uidentifiers. Dataset for NER and Vectorization.
* resources/entities.json ==> manual labels for NER entities. Contains span position (start, end), type (modifier/concept) 
and review_uid.
* NLP code test EN.pdf ==> code test explanation


 